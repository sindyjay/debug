<?php
/**
 * @version  php v.7.2
 * @version  Debug Class v.1.0.4
 * @author   Alejandro A. Shevyakov aka "Feanor Moriell" <sindyjay@yandex.ru> @ Limberia Development Group (LDGroup)
 * @license  GPL v.2
 * @package  LDGroup
 * @link     https://vk.com/id4907651
 */

namespace LDGroup;


use DateTime;
use Error;

/**
 * Class Debug
 * @package SJDevelopment
 */
class Debug
{
    private const DEBUG_FILE_DEFAULT_NAME           = "debug";
    private const DEBUG_FILE_EXTENSION              = "txt";
    private const DEBUG_FILE_PREFIX                 = "__";
    private const DEBUG_INLINE_DELIMITER            = ";;";
    private const DEBUG_NEW_LINE_PLACEHOLDER        = "{{EOL}}";
    private const DEBUG_RECORDS_DELIMITER_LENGTH    = 120;
    private const DEBUG_RECORDS_DELIMITER_SYMBOL    = "=";
    private const DEBUG_ENCODING_DEFAULT            = 0x0000;
    public const DEBUG_ENCODING_TO_JSON             = 0x0001;
    public const DEBUG_ENCODING_TO_JSON_TO_BASE64   = 0x0002;

    /**
     * @var string|null $_dateTime
     */
    private $_dateTime;

    /**
     * @var string|null $_debugFile
     */
    private $_debugFile;

    /**
     * @var string|null $_debugPathToFile
     */
    private $_debugPathToFile;

    /**
     * @var string|null $_comment
     */
    private $_comment;

    /**
     * @var mixed|null $_debugData
     */
    private $_debugData;

    /**
     * @var int $_encodingType;
     */
    private $_encodingType  = self::DEBUG_ENCODING_DEFAULT;

    /**
     * @var int $_defaultEncoding
     */
    private $_defaultEncoding = self::DEBUG_ENCODING_DEFAULT;

    /**
     * @var int[] $_encodingAliases
     */
    private $_encodingAliases = [
        "json" => self::DEBUG_ENCODING_TO_JSON,
        "base64" => self::DEBUG_ENCODING_TO_JSON_TO_BASE64
    ];

    /**
     * @var bool $_isDataEncoded
     */
    private $_isDataEncoded = false;

    /**
     * Дебаг данных любого типа с выводом результата в файл, либо на экран.
     * Настоятельно рекомендуется для каждого файла использовать какой-то один формат кодирования - особенно, если для сохраненного файла потребуется его
     * парсить.
     *
     * Если данные не являются строкой, они будут преобразованы при помощи встроенной в PHP функции print_r()
     *
     * @param mixed $debugData Любые данные
     * @param string $defaultEncoding одно из предусановленных значений - "json" или "base64". Если значение указано, то запуск метода Encode() будет
     * проигнорирован.
     */
    public function __construct($debugData = null, string $defaultEncoding = "")
    {
        $this->_setDateTime();
        $this->_setDebugFile(self::DEBUG_FILE_DEFAULT_NAME);
        if ($defaultEncoding !== '')
        {
            $this->_setDefaultEncodingByAlias($defaultEncoding);
            $this->_setEncodingType($this->_getDefaultEncoding());
        }
        $this->_setDebugData($debugData);
    }

    /**
     * Устанавливает имя для файла, в который требуется сохранить данные дебага.
     * Если не вызывать метод, будет использовано стандартное значение, либо установленное при последнем вызове этого метода в данном экземпляре класса.
     *
     * @param string $filename  Имя файла без расширения (подставляется само)
     * @return $this Контекст класса
     */
    final public function Filename(string $filename): Debug
    {
        if (trim($filename) !== '')
        {
            $this->_debugFile = "";
            $this->_setDebugFile($filename);
        }
        return $this;
    }

    /**
     * Формат кодирования устанавливается всякий раз, если нужно сохранить не "как есть".
     * Формат должен быть установлен ДО сохранения в файл или вывода на экран.
     * Сохранение в файл сбрасывает значение в "по умолчанию" (т.е. "как есть").
     *
     * Есть два варианта кодирования (публичные константы класса):
     *  - DEBUG_ENCODING_TO_JSON (0x0001) - запись данных строкой в формате JSON
     *  - DEBUG_ENCODING_TO_JSON_TO_BASE64 (0x0002) - преобразование данных сначала в JSON-Строку, а затем ее кодирование в Base64.
     *
     * Если метод не вызывать, подразумевается значение по умолчанию: "как есть", т.е. без кодирования.
     *
     * @param int $encodingTypeFlag см. публичные константы "DEBUG_ENCODING_TO_..."
     * @return Debug Контекст класса
     */
    final public function Encode(int $encodingTypeFlag): Debug
    {
        if ($this->_getDefaultEncoding() !== self::DEBUG_ENCODING_DEFAULT)
        {
            $encodingTypeFlag = $this->_getDefaultEncoding();
            if (count(debug_backtrace()) === 1)
            {
                return $this;
            }
        }
        $base64Encoded = null;
        $jsonEncoded = json_encode($this->_getDebugData());
        if (!$jsonEncoded)
        {
            throw new Error("Cannot make JSON from received Debug Data.");
        }
        if ($encodingTypeFlag === self::DEBUG_ENCODING_TO_JSON)
        {
            $this->_setDataIsEncoded(true);
            $this->_setEncodingType(self::DEBUG_ENCODING_TO_JSON);
            $this->ResetDebugData($jsonEncoded);
        }
        if ($encodingTypeFlag === self::DEBUG_ENCODING_TO_JSON_TO_BASE64)
        {
            $this->_setDataIsEncoded(true);
            $this->_setEncodingType(self::DEBUG_ENCODING_TO_JSON_TO_BASE64);
            $base64Encoded = base64_encode($jsonEncoded);
            $this->ResetDebugData($base64Encoded);
        }
        unset($jsonEncoded, $base64Encoded);
        return $this;
    }

    /**
     * При вызове метода данные дописываются в конец файла, определенного в данном экземпляре класса (если не указывать, будет использовано значение по
     * умолчанию).
     *
     * ВАЖНО! По окончании выполнения метода:
     *      1) обнуляется информация о кодировании. Если требуется сохранять данные в том же формате, следует это указать, вызвав метод Encode() с теми же
     *      параметрами - до вызова данного метода.
     *      2) обнуляется содержимое данных дебага. Используйте метод ResetDebugData()
     *
     * Комментарий обнуляется. Если передано пустое значение, то оно заменит уже сохраненное ранее.
     *
     * Если при создании экземпляра класса было установлено кодирование по умолчанию (кроме стандатного, когда кодирование не производится), то значение,
     * указанное в методе Encode(), будет проигнорировано.
     *
     * @param string $comment Опционально: однострочный коментарий к записи.
     */
    final public function Save(string $comment = ""): void
    {
        $this->_setComment($comment);
        $debugString = $this->_renderDebugString();
        if (
            ($this->_getDefaultEncoding() === self::DEBUG_ENCODING_DEFAULT)
            && $this->_getEncodingType() === self::DEBUG_ENCODING_DEFAULT
        )
        {
            $debugString .= $this->_getRecordsDelimiter();
        }

        $result = file_put_contents(
            $this->_getPathToDebugFile() . $this->_getDebugFile(),
            $debugString,
            FILE_APPEND
        );
        if ($result === false)
        {
            throw new Error(sprintf(
                "Cannot save string into debug file.%sFilename: %s%s.%sDebug Data: %s",
                PHP_EOL,
                $this->_getPathToDebugFile(),
                $this->_getDebugFile(),
                PHP_EOL,
                $debugString
            ));
        }
        unset($debugString, $result);
        $this->_setDataIsEncoded();
        $this->_setDebugData(null);
    }

    /**
     * Выводит на экран данные дебага. Форматирование происходит внутри HTML-тегов <pre></pre>. Данные независимо от формата предварительно форматируются
     * PHP-функцией print_r()
     *
     * ВАЖНО! В отличие от метода Save(), этот метод не обнуляет ни формат кодирования данных, ни содержимое переданных для дебага данных!
     *
     * Комментарий обнуляется. Если передано пустое значение, то оно заменит уже сохраненное ранее.
     *
     * @param string $comment  Необязательный параметр для вывода комментария к данным дебага.
     * @return Debug Контекст класса
     */
    final public function Write(string $comment = ""): Debug
    {
        $this->_setComment($comment);
        print sprintf(
            '<pre>%s%sDate & Time: %s%sEncoding type: %d%sComment: %s%s%s&gt;&gt;&gt; DEBUG SAY:%s%s%s%s%s%s%s</pre>%s',
            PHP_EOL,
            $this->_getRecordsDelimiter(),
            str_replace(self::DEBUG_INLINE_DELIMITER, " ", $this->_getDateTime()), PHP_EOL,
            $this->_getEncodingType(), PHP_EOL,
            $this->_getComment(), PHP_EOL, PHP_EOL, PHP_EOL, PHP_EOL, PHP_EOL, PHP_EOL,
            print_r($this->_getDebugData(), true), PHP_EOL,
            $this->_getRecordsDelimiter(), PHP_EOL
        );
        return $this;
    }

//    /**
//     * Это вспомогательный метод, который можно удалять.
//     * Его наличие никак не влияет на работу класса.
//     *
//     * @return string Небольшая справка о том, какой формат имеет запись в файле дебага. Формат применим для каждой отдельной записи.
//     */
//    public function HelpDebugStringFormat(): string
//    {
//        return sprintf(
//            "0. string Дата и время, с микросекундами%s1. int Тип кодирования%s2. string Комментарий к дебагу%s3. mixed (кодированные или нет) данные, переданные на дебаг в экземпляр класса",
//            PHP_EOL,
//            PHP_EOL,
//            PHP_EOL
//        );
//    }

    // TODO implement GetFromFile(string $debugFile = self::DEBUG_FILE_DEFAULT_NAME)

//    /**
//     * Пробует разобрать сохраненный файл дебага. Если файл собран верно, то вернет данные в виде индексированного массива. В случае ошибки на любом этапе
//     * разбора файле, вернет массив вида [FALSE].
//     *
//     * @param string $debugFile Имя файла( без расширения), данные которого надо попытаться обработать и вернуть.
//     * @return array Массив, где каждый элемент - массив с разобранной одной записью из файла дебага.
//     */
//    public function GetFromFile(string $debugFile = self::DEBUG_FILE_DEFAULT_NAME): array
//    {
//        return ["THE METHOD IS NOT IMPLEMENTED YET"];
//    }

    /**
     * Обнуляет существующее значение данных дебага и помещает взамен то, что переданы в качестве единственного аргумента.
     *
     * @param mixed $newDebugData  Новые данные для дебага, которые заменять предыдущие.
     * @return Debug Контекст класса.
     */
    final public function ResetDebugData($newDebugData): Debug
    {
        $this->_setDebugData(null);
        $this->_setDebugData($newDebugData);
        return $this;
    }

    private function _renderDebugString(): string
    {
        if ($this->_getDefaultEncoding() !== self::DEBUG_ENCODING_DEFAULT)
        {
            $this->Encode($this->_getDefaultEncoding());
        }
        return sprintf(
            '%s%s%s%s%s%s%s%s',
            $this->_getDateTime(),
            self::DEBUG_INLINE_DELIMITER,
            $this->_getEncodingType(),
            self::DEBUG_INLINE_DELIMITER,
            $this->_getComment(),
            self::DEBUG_INLINE_DELIMITER,
            is_string($this->_getDebugData())
                ? $this->_getDebugData()
                : print_r($this->_getDebugData(), true),
            PHP_EOL
        );
    }

    private function _getRecordsDelimiter(): string
    {
        $rd = "";
        $max = self::DEBUG_RECORDS_DELIMITER_LENGTH + 1;
        $rd .= str_repeat(self::DEBUG_RECORDS_DELIMITER_SYMBOL, $max);
        unset ($max);
        return sprintf(
            "%s%s%s%s",
            PHP_EOL,
            $rd,
            PHP_EOL,
            PHP_EOL
        );
    }

    private function _setDateTime(): void
    {
        $dt = new DateTime();
        $this->_dateTime = $dt->format(
            'd.m.y'
            . self::DEBUG_INLINE_DELIMITER
            .'H:i:s'
            . self::DEBUG_INLINE_DELIMITER .'u'
        );
        unset($dt);
    }

    private function _getDateTime(): string
    {
        return $this->_dateTime;
    }

    private function _setDebugFile(string $filename): void
    {
        $s1 = "";
        if (strpos($filename, "\\") !== false)
        {
            $s1 = "\\";
        }
        elseif (strpos($filename, "/") !== false)
        {
            $s1 = "/";
        }
        $filename = trim(str_replace(PHP_EOL, "_", $filename));
        if ($s1 !== "")
        {
            $pathValues = explode($s1, $filename);
            if ($pathValues === false)
            {
                throw new Error("System Directory Separator contain empty value. Under these conditions, the program cannot be executed.");
            }
            $counter = count($pathValues);
            $pathToFile = "";
            if ( $counter > 0 )
            {
                for ($i = 0; $i < ($counter - 1); $i++)
                {
                    $pathToFile .= ($pathValues[$i] !== DIRECTORY_SEPARATOR)
                        ? ($pathValues[$i] . DIRECTORY_SEPARATOR)
                        : $pathToFile[$i];
                }
                $filename = $pathValues[($counter - 1)];
            }
            if (!is_dir($pathToFile))
            {
                throw new Error("The specified directory probably does not exist. Please make sure that the path to the saved file is correct. All directories must exist.");
            }
            $this->_setPathToDebugFile($pathToFile);
        }
        $nameMaxLength = 254 - strlen((int)self::DEBUG_FILE_EXTENSION);
        if (strlen($filename) > $nameMaxLength)
        {
            $filename = substr($filename, 0, $nameMaxLength);
        }
        $this->_debugFile =
            $filename === ''
                ? sprintf("%s%s.%s", self::DEBUG_FILE_PREFIX, self::DEBUG_FILE_DEFAULT_NAME, self::DEBUG_FILE_EXTENSION)
                : sprintf("%s%s.%s", self::DEBUG_FILE_PREFIX, $filename, self::DEBUG_FILE_EXTENSION);
        unset($s1, $pathValues, $counter, $pathToFile, $nameMaxLength);
    }

    private function _getDebugFile(): string
    {
        return $this->_debugFile;
    }

    private function _setPathToDebugFile(string $path = ""): void
    {
        $this->_debugPathToFile = trim($path);// TODO: сделать проверку валидности пути
    }

    private function _getPathToDebugFile(): string
    {
        return $this->_debugPathToFile ?? "";
    }

    private function _setComment(string $comment): void
    {
        $this->_comment = trim(str_replace(PHP_EOL, self::DEBUG_NEW_LINE_PLACEHOLDER, $comment));
    }

    private function _getComment(): string
    {
        return $this->_comment;
    }

    private function _setDebugData($debugData): void
    {
        $this->_debugData = $debugData;
    }

    /**
     * @return mixed|null
     */
    private function _getDebugData()
    {
        return $this->_debugData;
    }

    private function _setEncodingType(int $encodingType): void
    {
        if ($encodingType === self::DEBUG_ENCODING_TO_JSON)
        {
            $this->_encodingType = self::DEBUG_ENCODING_TO_JSON;
        }
        elseif ($encodingType === self::DEBUG_ENCODING_TO_JSON_TO_BASE64)
        {
            $this->_encodingType = self::DEBUG_ENCODING_TO_JSON_TO_BASE64;
        }
        else
        {
            $this->_encodingType = self::DEBUG_ENCODING_DEFAULT;
        }
    }

    private function _getEncodingType(): int
    {
        return $this->_encodingType;
    }

    private function _setDataIsEncoded(bool $state = false): void
    {
        $this->_isDataEncoded = $state;
    }

//    private function _getDataIsEncoded(): bool
//    {
//        return $this->_isDataEncoded;
//    }

    private function _setDefaultEncoding(int $encoding): void
    {
        $this->_defaultEncoding = $encoding;
    }

    private function _getDefaultEncoding(): int
    {
        return $this->_defaultEncoding;
    }

    private function _setDefaultEncodingByAlias(string $alias): void
    {
        $this->_setDefaultEncoding($this->_getEncodingByAlias($alias));
    }

    private function _getEncodingByAlias(string $alias): int
    {
        foreach ($this->_encodingAliases as $key => $value)
        {
            if (trim($alias) === $key)
            {
                return $value;
            }
        }
        return self::DEBUG_ENCODING_DEFAULT;
    }

    /**
     * @param mixed $info any exception type
     * @return array Detailed info from received Exception
     */
    public static function FormatException($info): array
    {
        return [
            "message"        => $info->getMessage() ?? "EXCEPTION OCCURRED",
            "code"           => $info->getCode()    ?? 0,
            "file"           => $info->getFile()    ?? "unknown file",
            "line"           => $info->getLine()    ?? 0,
            "stackTrace"     => $info->getTrace()   ?? [],
            "exception type" => gettype($info)
        ];
    }

}
